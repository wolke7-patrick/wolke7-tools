pipeline {
    triggers{
        bitbucketPush()
    }
    options {
        buildDiscarder(logRotator(numToKeepStr: '10'))
        disableConcurrentBuilds()
	timeout(time: 30, unit: 'MINUTES', activity: true)
    }
    parameters {
        booleanParam(name: "RANCHER", description: "select if you want to deploy RANCHER", defaultValue: true)
        booleanParam(name: "KUBERNETES_DASHBOARD", description: "select if you want to deploy KUBERNETES_DASHBOARD", defaultValue: true)
        booleanParam(name: "PORTAINER", description: "select if you want to deploy PORTAINER", defaultValue: true)
        booleanParam(name: "PROMETHEUS", description: "select if you want to deploy PROMETHEUS", defaultValue: true)
        booleanParam(name: "GRAFANA", description: "select if you want to deploy GRAFANA", defaultValue: true)
        booleanParam(name: "FAIRWINDS", description: "select if you want to deploy FAIRWINDS", defaultValue: true)
    }
    agent {
        kubernetes {
            yamlFile 'k8s/build-environment.yaml'
            // workspaceVolume hostPathWorkspaceVolume() (doesn't work as kubernetes is not on the same host :-()
        }
    }
    environment {
        DEBUG = false
        VERIFY_CHECKSUM = false
        PATH = "/build:$PATH"
        DOMAIN = "ripro.tk"
    }
    stages {
        stage('verifyPrerequisites') {
            steps {
                container('eksctl') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                        script {
                            sh 'eksctl get clusters'
                            sh 'eksctl utils write-kubeconfig --cluster=wolke7Cluster'
			                echo "install helm..."
                            sh "wget -O install-helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 2> /dev/null && sed -i 's,bash,sh,g' install-helm.sh && chmod +x install-helm.sh && ./install-helm.sh"
                            sh 'helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/'
                            sh 'helm repo add portainer https://portainer.github.io/k8s/'
                            sh 'helm repo add bitnami https://charts.bitnami.com/bitnami'
                            sh 'helm repo add fairwinds-stable https://charts.fairwinds.com/stable'
			    sh 'helm repo add rancher-stable https://releases.rancher.com/server-charts/stable'
			    sh 'helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx'
                            sh 'helm repo update'
                        }
                    }
                }
            }
        }
        stage('getFSID') {
            steps {
                container('awscli') {
                  withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                      script {
                          env.FSID = sh ( script: "aws efs describe-file-systems --creation-token wolke7-efs --query 'FileSystems[*].FileSystemId' --output text", returnStdout: true ).trim()
                          echo "FSID: ${env.FSID}"
                      }
                  }
                }
            }
        }
        stage('createPVs') {
            steps {
                container('eksctl') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                        script {
                            sh "sed -i \"s,volumeHandle: fs-4af69aab,volumeHandle: ${FSID},g\" pvs/pv-*.yaml"
                            sh "kubectl apply -f pvs/storageclass.yaml"
                        }
                    }
                }
            }
	    }  
        stage('deployRancher') {
            when {
                expression { params.RANCHER == true }
            }
            steps {
                container('eksctl') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                        script {
			    sh 'helm upgrade --install --create-namespace -n cattle-system rancher rancher-stable/rancher --set rancherImagePullPolicy=Always --set tls=external --set hostname=rancher.ripro.tk'
			    // sh 'kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.0.4/cert-manager.crds.yaml'
                            sh 'helm upgrade --install ingress-nginx ingress-nginx/ingress-nginx --namespace ingress-nginx --set controller.service.type=LoadBalancer --version 3.12.0 --create-namespace'
                        }
                    }
                }
            }
        }
        stage('deployKubernetesDashboard') {
            when {
                expression { params.KUBERNETES_DASHBOARD == true }
            }
            steps {
                container('eksctl') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                        script {
                            sh 'helm upgrade --install --create-namespace -n kubernetes-dashboard kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard --set service.type=LoadBalancer --set protocolHttp=true --set service.externalPort=80 --set metrics-server.enabled=true --set resources.requests.cpu=100m,resources.requests.memory=128Mi,resources.limits.cpu=150m,resources.limits.memory=256Mi --set image.pullPolicy=Always'
                            sh 'kubectl apply -f wolke7-kubernetes-dashboard/crb.yaml -n kubernetes-dashboard'
                        }
                    }
                }
            }
        }
        stage('deployPortainer') {
            when {
                expression { params.PORTAINER == true }
            }
            steps {
                container('eksctl') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                        script {
                            sh 'kubectl create ns portainer || true'
                            sh "kubectl apply -f pvs/pv-portainer.yaml"
                            sh "kubectl apply -f pvs/pvc-portainer.yaml -n portainer"
                            sh 'helm upgrade --install --create-namespace -n portainer portainer portainer/portainer --set image.tag=2.1.1 --set service.type=LoadBalancer --set service.httpPort=80 --set persistence.existingClaim=pvc-portainer --set resources.requests.cpu=100m,resources.requests.memory=128Mi,resources.limits.cpu=150m,resources.limits.memory=256Mi '
                        }
                    }
                }
            }
        }
        stage('deployPrometheus') {
            when {
                expression { params.PROMETHEUS == true }
            }
            steps {
                container('eksctl') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                        script {
                            sh 'helm upgrade --install --create-namespace prometheus bitnami/kube-prometheus -n prometheus --set prometheus.service.type=LoadBalancer --set prometheus.service.port=80 --set prometheus.thanos.create=true --set operator.resources.requests.cpu=100m,operator.resources.requests.memory=256Mi,operator.resources.limits.cpu=150m,operator.resources.limits.memory=256Mi --set prometheus.resources.requests.cpu=150m,prometheus.resources.requests.memory=512Mi,prometheus.resources.limits.cpu=300m,prometheus.resources.limits.memory=512Mi --set alertmanager.resources.requests.cpu=50m,alertmanager.resources.requests.memory=128Mi,alertmanager.resources.limits.cpu=150m,alertmanager.resources.limits.memory=128Mi --set alertmanager.image.pullPolicy=Always,operator.image.pullPolicy=Always,prometheus.image.pullPolicy=Always,global.imagePullPolicy=Always'
                        }
                    }
                }
            }
        }
        stage('deployGrafana') {
            when {
                expression { params.GRAFANA == true }
            }
            steps {
                container('eksctl') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                        script {
                            sh 'kubectl create ns prometheus || true'
			                sh "kubectl apply -f pvs/pv-grafana.yaml"
			                sh "kubectl apply -f pvs/pvc-grafana.yaml -n prometheus"
                            sh 'helm upgrade --install --create-namespace grafana bitnami/grafana -n prometheus --set service.type=LoadBalancer --set service.port=80 --set persistence.existingClaim=pvc-grafana --set resources.requests.cpu=100m,resources.requests.memory=256Mi,resources.limits.cpu=150m,resources.limits.memory=256Mi --set image.pullPolicy=Always'
                        }
                    }
                }
            }
        }
        stage('deployFairwindsTools') {
            when {
                expression { params.FAIRWINDS == true }
            }
            steps {
                container('eksctl') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                        script {
                            sh 'helm upgrade --install --create-namespace polaris fairwinds-stable/polaris --namespace polaris --set dashboard.service.type=LoadBalancer,dashboard.service.port=80'
                            sh 'helm upgrade --install --create-namespace goldilocks fairwinds-stable/goldilocks --namespace goldilocks --set dashboard.service.type=LoadBalancer,dashboard.service.port=80'
                            sh 'helm upgrade --install --create-namespace gemini fairwinds-stable/gemini --namespace gemini'
                            echo "install k8s volume snapshots"
                            sh 'kubectl apply -f https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/53469c21962339229dd150cbba50c34359acec73/config/crd/snapshot.storage.k8s.io_volumesnapshotclasses.yaml'
                            sh 'kubectl apply -f https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/53469c21962339229dd150cbba50c34359acec73/config/crd/snapshot.storage.k8s.io_volumesnapshotcontents.yaml'
                            sh 'kubectl apply -f https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/53469c21962339229dd150cbba50c34359acec73/config/crd/snapshot.storage.k8s.io_volumesnapshots.yaml'
                            sh 'kubectl apply -f https://raw.githubusercontent.com/kubernetes-csi/external-snapshotter/master/deploy/kubernetes/snapshot-controller/setup-snapshot-controller.yaml'
                            echo "define volume snapshot class (store snapshots in efs)"
                            sh 'kubectl apply -f pvs/volumesnapshotclass.yaml '
                            echo "define annotations to skip polaris checks"
                            sh 'kubectl annotate ds -n kube-system aws-node polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate ds -n kube-system aws-node-termination-handler polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate ds -n kube-system efs-csi-node polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate ds -n kube-system kube-proxy polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate deploy -n kube-system cluster-autoscaler polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate deploy -n kube-system coredns polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate deploy -n kubernetes-dashboard kubernetes-dashboard polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate deploy -n kubernetes-dashboard kubernetes-dashboard-metrics-server polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate deploy -n portainer portainer polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate alertmanager -n prometheus prometheus-kube-prometheus-alertmanager polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate ds -n prometheus prometheus-node-exporter polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate deploy -n prometheus prometheus-kube-prometheus-operator polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate deploy -n prometheus prometheus-kube-state-metrics polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate prometheus -n prometheus prometheus-kube-prometheus-prometheus polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate deploy -n prometheus grafana polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate deploy -n gemini gemini-controller polaris.fairwinds.com/exempt=true --overwrite'
                            sh 'kubectl annotate deploy -n goldilocks goldilocks-controller polaris.fairwinds.com/exempt=true --overwrite'
                        }
                    }
                }
            }
        }
        stage('getServiceUrls') {
            steps {
                container('eksctl') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                        script {
                            sh 'sleep 30'
                            echo "rancher"
                            sh 'kubectl get svc -n ingress-nginx'
                            env.RANCHER_SERVICE_TARGET = sh ( script: "kubectl get svc --namespace ingress-nginx ingress-nginx-controller --template '{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}'", returnStdout: true ).trim()
            			    sh "echo ${RANCHER_SERVICE_TARGET}"

                            echo "kubernetes-dashboard"
                            sh 'kubectl get svc -n kubernetes-dashboard'
                            env.KUBERNETES_DASHBOARD_SERVICE_TARGET = sh ( script: "kubectl get svc --namespace kubernetes-dashboard kubernetes-dashboard --template '{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}'", returnStdout: true ).trim()
            			    sh "echo ${KUBERNETES_DASHBOARD_SERVICE_TARGET}"

                            echo "portainer"
                            sh 'kubectl get svc -n portainer'
                            env.PORTAINER_SERVICE_TARGET = sh ( script: "kubectl get svc --namespace portainer portainer --template '{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}'", returnStdout: true ).trim()
	            		    sh "echo ${PORTAINER_SERVICE_TARGET}"

                            echo "prometheus"
                            sh 'kubectl get svc -n prometheus'
                            env.PROMETHEUS_SERVICE_TARGET = sh ( script: "kubectl get svc --namespace prometheus prometheus-kube-prometheus-prometheus --template '{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}'", returnStdout: true ).trim()
            			    sh "echo ${PROMETHEUS_SERVICE_TARGET}"

                            echo "grafana"
                            sh 'kubectl get svc -n prometheus'
                            sh 'echo http://\$(kubectl get svc --namespace prometheus grafana --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}")'
                            env.GRAFANA_SERVICE_TARGET = sh ( script: "kubectl get svc --namespace prometheus grafana --template '{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}'", returnStdout: true ).trim()
            			    sh "echo ${GRAFANA_SERVICE_TARGET}"

                            echo "polaris"
                            sh 'kubectl get svc -n polaris'
                            env.POLARIS_SERVICE_TARGET = sh ( script: "kubectl get svc --namespace polaris polaris-dashboard --template '{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}'", returnStdout: true ).trim()
            			    sh "echo ${POLARIS_SERVICE_TARGET}"

                            echo "goldilocks"
                            sh 'kubectl get svc -n goldilocks'
                            env.GOLDILOCKS_SERVICE_TARGET = sh ( script: "kubectl get svc --namespace goldilocks goldilocks-dashboard --template '{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}'", returnStdout: true ).trim()
            			    sh "echo ${GOLDILOCKS_SERVICE_TARGET}"

                        }
                    }
                }
            }
	}
        stage('updateAwsRoute53DnsRecords') {
            steps {
                container('awscli') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                        script {
                            echo "update route53 rsource-record-set for rancher"
                            sh "sed \"s,SERVICE_TARGET,${RANCHER_SERVICE_TARGET},g\" aws/change-record-set.json | sed \"s,SERVICE,rancher,g\" |  xargs -0 aws route53 change-resource-record-sets --hosted-zone-id \"/hostedzone/Z04511981P74GWKFKSR9Q\" --change-batch"

                            echo "update route53 rsource-record-set for kubernetes-dashboard"
                            sh "sed \"s,SERVICE_TARGET,${KUBERNETES_DASHBOARD_SERVICE_TARGET},g\" aws/change-record-set.json | sed \"s,SERVICE,kubernetes-dashboard,g\" |  xargs -0 aws route53 change-resource-record-sets --hosted-zone-id \"/hostedzone/Z04511981P74GWKFKSR9Q\" --change-batch"

                            echo "update route53 rsource-record-set for portainer"
                            sh "sed \"s,SERVICE_TARGET,${PORTAINER_SERVICE_TARGET},g\" aws/change-record-set.json | sed \"s,SERVICE,portainer,g\" |  xargs -0 aws route53 change-resource-record-sets --hosted-zone-id \"/hostedzone/Z04511981P74GWKFKSR9Q\" --change-batch"

                            echo "update route53 rsource-record-set for prometheus"
                            sh "sed \"s,SERVICE_TARGET,${PROMETHEUS_SERVICE_TARGET},g\" aws/change-record-set.json | sed \"s,SERVICE,prometheus,g\" |  xargs -0 aws route53 change-resource-record-sets --hosted-zone-id \"/hostedzone/Z04511981P74GWKFKSR9Q\" --change-batch"

                            echo "update route53 rsource-record-set for grafana"
                            sh "sed \"s,SERVICE_TARGET,${GRAFANA_SERVICE_TARGET},g\" aws/change-record-set.json | sed \"s,SERVICE,grafana,g\" |  xargs -0 aws route53 change-resource-record-sets --hosted-zone-id \"/hostedzone/Z04511981P74GWKFKSR9Q\" --change-batch"

                            echo "update route53 rsource-record-set for polaris"
                            sh "sed \"s,SERVICE_TARGET,${POLARIS_SERVICE_TARGET},g\" aws/change-record-set.json | sed \"s,SERVICE,polaris,g\" |  xargs -0 aws route53 change-resource-record-sets --hosted-zone-id \"/hostedzone/Z04511981P74GWKFKSR9Q\" --change-batch"

                            echo "update route53 rsource-record-set for goldilocks"
                            sh "sed \"s,SERVICE_TARGET,${GOLDILOCKS_SERVICE_TARGET},g\" aws/change-record-set.json | sed \"s,SERVICE,goldilocks,g\" |  xargs -0 aws route53 change-resource-record-sets --hosted-zone-id \"/hostedzone/Z04511981P74GWKFKSR9Q\" --change-batch"

                        }
                    }
                }
            }
	}
        stage('displayLinks') {
            steps {
                container('awscli') {
                    withAWS(region: 'eu-central-1', credentials: 'ripro-aws') {
                            echo "rancher"
                            echo "http://rancher.${DOMAIN}/"
                            echo "kubernetes-dashboard"
                            echo "http://kubernetes-dashboard.${DOMAIN}/"
                            echo "portainer"
                            echo "http://portainer.${DOMAIN}/"
                            echo "prometheus"
                            echo "http://prometheus.${DOMAIN}/"
                            echo "grafana"
                            echo "http://grafana.${DOMAIN}/"
                            echo "polaris"
                            echo "http://polaris.${DOMAIN}/"
                            echo "goldilocks"
                            echo "http://goldilocks.${DOMAIN}/"
                    }
                }
            }
	}
    }
}
